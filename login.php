<?php
session_start();

if ($_SESSION['usuario']['active'])
    header("Location: index.php");

require_once "classes/Auth.php";
require_once "classes/Conexao.php";

if ($usuario = Auth::_auth_user($_POST['login'], $_POST['senha'])[0]) //Autenticação do usuário, testa o cpf ou o email ou o nome de usuário (Usuario_Usuario)
{
    $_SESSION['usuario'] = $usuario; //grava dados do usuário na variável de sessão para futuros usos
    $_SESSION['usuario']['active'] = true; //variável de controle

    switch ($usuario['Tipo_Usuario_Id_Tipo_Usuario']) { //aqui voce pode inserir páginas que um tipo de usuário não pode acessar, colocando na lista (array)
        case 1:
        case "1":
            $_SESSION['usuario']['deny'] = ["CreateAtividade", "CreateAlarme"];
            break;
        case 2:
        case "2":
            $_SESSION['usuario']['deny'] = [""];
            break;
        default:
            $_SESSION['usuario']['errors'] = [["Tipo_Usuario_Id_Tipo_Usuario switch failed!" => $usuario['Tipo_Usuario_Id_Tipo_Usuario']]];
    }


    header("Location: index.php");//redireciona para a tela inicial, e
}else{ //Caso autenticação falhe, redirecionar para página de login com erro
    header("Location: login.html?input_mismatch");
}