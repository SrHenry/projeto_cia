<?php
session_start();

if (!isset($_SESSION['usuario']['active']) || !($_SESSION['usuario']['active']))
    header("Location: index.php");

include("conexao.php");

$consulta = "SELECT * FROM rotina";
$con = $link->query($consulta) or die($link->error);
?> 


<!DOCTYPE html>
<html lang="pt-br">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="bootstrap-4.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="normalize.css">
    <link rel="stylesheet" href="padrao_cia_stylesheet.css">
    <link rel="stylesheet" href="checklist_atividade_familiar_stylesheet.css">
    <title>CIA - Checklist Atividades</title>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript">
        var colorList = {
            count: {
                c : 0,
                next: function() {
                    if (this.c < colorList.colors.length-1)
                        return ++(this.c);
                    else {
                        return (this.c = 0);
                    }
                }
            },
            colors: [
                "#fa6879",
                "#e9ff8c",
                "#3bb964"
            ],
            nextColor: function() {
                return this.colors[this.count.next()];
            }
        };
        function selecionar(k){
            var x = document.getElementsByClassName("camaleao");
            k.style.background = colorList.nextColor();

//            var cor="#fa6879";
//            if( cor === "#fa6879"){
//                document.getElementById(k).style.background = "#3bb964";
//            };
//            if( x === "blue"){
//                document.getElementById(k).style.background = "red";
//            };
        }
    </script>

    <title>Title</title>
</head>
<body>

    <nav id="menu">
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">Menu</a></li>
        </ul>
    </nav>
    <center>
    <table id="tabelaAtividade" style="position: relative; top: 100px; ">
        <tr>
            <th style="width:5%"></th>
            <th style="width:5%"></th>
            <th style="width:70%">Atividade</th>
            <th style="width:10%">Horário</th>
            <th style="width:10%">Status</th>
        </tr>
        <tr>
            <td><img class="imgTabela" src="imagem/cancelar.png"></td>
            <td><img class="imgTabela" src="imagem/alterar.png"></td>
            <td>Almoço </td>
            <td>12:30</td>
            <td><div onclick="selecionar(this);" class="camaleao" id="div_1"></div></td>
        </tr>
        <tr>
            <td><img class="imgTabela" src="imagem/cancelar.png"></td>
            <td><img class="imgTabela" src="imagem/alterar.png"></td>
            <td>Banho</td>
            <td>13:00</td>
            <td ><div onclick="selecionar(this);" class=" camaleao" id="div_2"></div></td>
        </tr>
        
        <?php while ($dado = $con-> fetch_array()){ ?>
        <tr>
            <td><img class="imgTabela" src="imagem/cancelar.png"></td>
            <td><img class="imgTabela" src="imagem/alterar.png"></td>
            <td><?php echo $dado["Nome_Rotina"]; ?></td>
            <td><?php echo $dado["Hora_Rotina"]; ?></td>
            <td><div onclick="selecionar(this.id);" class=" camaleao" id="div_3"></div></td>
        </tr>
        <?php } ?>
        

    </table>

    <?php if (isset($_SESSION['usuario']['deny']) && !empty($_SESSION['usuario']['deny']) && in_array("CreateAtividade", $_SESSION['usuario']['deny'])) {}else{?>
        <table id="tabelaAdicionarAtividade" style="position: relative; top: 100px;">
            <tr>
                <th style="width:30%"></th>
                <th style="width:70%"></th>
            </tr>
            <form style="position: relative; top:100px;" method="post" >
                <tr>
                    <td>
                        <label for="novaatividade">Nome da nova atividade:</label>
                        <input type="text" class="form-control" id="novaatividade" name="Nome_Rotina" placeholder="Digite o nome da atividade">
                    </td>
                    <td>
                        <label for="novaatividade">Horário da atividade:</label>
                        <input type="datetime" class="form-control" id="novaatividade" name="Hora_Rotina" placeholder="Digite o horário da atividade">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label for="novaatividade">Observação:</label>
                        <input type="text" class="form-control" id="novaatividade" name="Descricao_Rotina" placeholder="Alguma observação?">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button type="submit" class="btn btn-lg btn-block btn-success" id="botao_confirmar">Cadastrar</button>
                    </td>
                </tr>
            </form>
        </table>
    <?php }?>

    </center>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
