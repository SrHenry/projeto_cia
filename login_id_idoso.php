<?php

include("conexao.php");

$consulta = "SELECT * FROM tipo_usuario";
$con = $link->query($consulta) or die($link->error);
 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="bootstrap-4.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="normalize.css">
    <link rel="stylesheet" href="padrao_cia_stylesheet.css">


    <!--[if IE 9]>
    <script src="html5shiv.js"></script>
    <![endif]-->
    <title>CIA - Login</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
    <script src="http://digitalbush.com/files/jquery/maskedinput/rc3/jquery.maskedinput.js" type="text/javascript"></script>
    <script>
        jQuery(function($){
           $(".datas").mask("99/99/9999");
           $("#cpf").mask("999.999.999-99");
        });
    </script>
  
</head>
<body>
    <nav id="menu">
        <ul >
            <li><a href="paginaInicial.html">Home</a></li>
            <li><a href="#">Menu</a></li>
        </ul>
    </nav>

    <div class="container">
        
        <form class=" formulario" action="cadastro_usuario.php" method="post">
            <center> <h1 id="cadastrar">Cadastre-se</h1></center>
            <br /> <br /> 
            <div class="row">
                <div class="col-6 formulario">
                    <label for="id_idoso">Nome:</label>
                    <input required= "" type="text" class="form-control"  name="Nome_Usuario" placeholder="Digite seu nome">

                    <label for="id_idoso">Data de nascimento:</label>
                    <input required="" type="date" class="form-control" name="Data_Nasc_Usuario" placeholder="Digite seu nome">

                    <label for="id_idoso">Crie um nome para login:</label>
                    <input required="" type="text" class="form-control"  name="Usuario_Usuario" placeholder="Digite seu nome de login">

                </div>
                <div class="col-6 formulario">
                    <label for="id_idoso">Email:</label>
                    <input required="" type="email" class="form-control"  name="Email_Usuario" placeholder="Digite seu email">

                    <label for="id_idoso">CPF:</label>
                    <input required="" type="text" class="form-control" id="cpf" name="CPF_Usuario"  placeholder="Digite seu CPF">

                    <label for="id_idoso">Crie uma senha:</label>
                    <input required="" type="password" class="form-control"  name="Senha_Usuario" placeholder="Digite sua senha">

                </div>
                <div class="col-12 formulario">
                    <center>
                        <label ><strong>Qual é a sua relação com o idoso:</strong></label>
                           
                            <?php while ($dado = $con-> fetch_array()){
                                    //echo $dado;
                             ?>

                                <input type="radio" id="tipo_usuario_cpi " class="radiobutton" name="Tipo_Usuario" value="<?php echo $dado["Id_Tipo_Usuario"]; ?>"> 

                                <label  for="tipo_usuario_cpi" > 
                            <?php echo $dado["Tipo_Usuario_Atributo"]; 
                             } ?>
                            
                        </label>
            
                    </center>
                </div>

    

                <div class="col-3 formulario"> <a href="login_id_idoso.php"><button type="submit" class="btn btn-lg btn-block btn-success" id="botao_cancelar">Cancelar</button><a/></div>
                <div class="col-3"></div>
                <div class="col-3"></div>
                <div class="col-3 formulario"> <a href="consulta.php"> <button type="submit" name="Cadastrar Usuario"class="btn btn-lg btn-block btn-success" id="botao_confirmar">Cadastrar</button></a> </div>
            </div>

        </form>
        
    </div>

    <img id="img_fundo" class="esquerda" src="imagem/drug.svg">
    <div id="corum">
    </div>
    <div id="cordois">
    </div>
    <div id="cortres">
    </div>
    <div id="corquatro">
    </div>
</body>
</html>