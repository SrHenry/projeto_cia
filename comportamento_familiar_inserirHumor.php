<?php

include("conexao.php");

$consulta = "SELECT * FROM Tipo_Comportamento";
$con = $link->query($consulta) or die($link->error);
 
?>
<!DOCTYPE html>
<html>
<head>
     <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="bootstrap-4.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="normalize.css">
    <link rel="stylesheet" href="padrao_cia_stylesheet.css">
    <link rel="stylesheet" href="padrao_comportamento.css">



    <!--[if IE 9]>
    <script src="html5shiv.js"></script>
    <![endif]-->
    <title>CIA - Login</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
    <script src="http://digitalbush.com/files/jquery/maskedinput/rc3/jquery.maskedinput.js" type="text/javascript"></script>

    <title></title>
</head>
<body>

     <nav id="menu">
        <ul >
            <li><a href="paginaInicial.html">Home</a></li>
            <li><a href="#">Menu</a></li>
        </ul>
    </nav>
    <div class="container">
        <form style="position: relative; top:100px;" method="post" action="conexao_comportamento_familiar_inserirHumor.php">


            <div class="row">
                <div class="col-12">
                    <center>
                        <label ><strong>Insira a data da análise:</strong></label>
                        <input type="date" id="data_comportamento" class="form-control" name="Data_Comportamento" value="<?php date_default_timezone_set("America/Porto_Velho"); echo date("Y-m-d");?>" required>
                    </center><br/>
                </div>
            </div>


           <div class="row">
                <div class="col-12">
                    <center>
                    <label for="novocomportamento"><strong>Observações do comportamento:</strong></label>
                    <input type="text" class="form-control" id="novocomportamento" name="Nome_Comportamento" placeholder="Observações sobre o comportamento do idoso">
                    </center>
                </div>
            </div>
            
            <div class="row">
                <div class="col-12">
                    <center><br/>
                        <label ><strong>Qual o humor do Idoso:</strong></label>
                               
                            <?php $__count = 0; while ($dado = $con-> fetch_array()){
                                            //echo $dado;
                                $__count++;
                            ?>

                             <input type="radio" id="tipo_comportamento<?php echo $__count?>" class="radiobutton" name="tipo_comportamento" value="<?php echo $dado["Id_Tipo_Comportamento"]?>" required> 

                            <label  for="tipo_comportamento<?php echo $__count?>" > 
                                <?php echo $dado["Nome_Comportamento"];?> 
                            </label>
                        <?php
                            } ?>
                                
                
                    </center>
                </div>
            </div>



          <div class="row">
                <div class="col-12">
                    <center><br/>
                    <label ><strong>Qual o turno que foi observado o humor do Idoso:</strong></label>
                        <input type="radio" id="turno_comportamento1" class="radiobutton" name="Turno_Comportamento" value="Manhã" required="required"> <label for="turno_comportamento1">Manhã</label>
                        <input type="radio" id="turno_comportamento2" class="radiobutton" name="Turno_Comportamento" value="Tarde" required="required"> <label for="turno_comportamento2">Tarde</label>
                        <input type="radio" id="turno_comportamento3" class="radiobutton" name="Turno_Comportamento" value="Noite" required="required"> <label for="turno_comportamento3">Noite</label>
                        <input type="radio" id="turno_comportamento4" class="radiobutton" name="Turno_Comportamento" value="Dia todo" required="required"> <label for="turno_comportamento4">Dia todo</label>
                    </center><br/>
                </div>
            </div>



            <div class="row">
                <div class="col-6">
                     <a href="comportamento_familiar_telainicial.php"> <button type="button"  class="btn btn-lg btn-block btn-success botao_voltar" >Voltar</button></a>
                </div>
                <div class="col-6">
                     <button type="submit" class="btn btn-lg btn-block btn-success" id="botao_confirmar">Adicionar</button>
                </div>
            </div>

        </form>
    </div>    
</body>
</html>