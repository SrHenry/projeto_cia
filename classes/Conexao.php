<?php
/**
 * Created by PhpStorm.
 * User: k4io_
 * Date: 04/10/2018
 * Time: 15:29
 */

class Conexao
{

    private $user = 'root';
//    private $senha = 'Gotec.nodo01';
    private $senha = '';
    private $db = 'projeto_cia';
    private $host = 'localhost';
    private $port = "3306";

    /**
     * Conexao constructor.
     * @param string $user
     * @param string $senha
     * @param string $db
     * @param string $host
     * @param string $port
     */
    public function __construct($user = 'root', $senha = '', $db = 'projeto_cia', $host = 'localhost', $port = "3306")
    {
        $this->user = $user;
        $this->senha = $senha;
        $this->db = $db;
        $this->host = $host;
        $this->port = $port;
    }

    /**
     * @return PDO|string
     */



    public function conecta()
    {
        return $this->_conecta($this->user, $this->senha);
    }

    public function _conecta($user, $passwd)
    {
        $this->host = $this->host.":".$this->port;
//        try {
            $con = new PDO("mysql:host=$this->host;dbname=$this->db", $user, $passwd/*, array(PDO::ATTR_PERSISTENT => true,
                PDO::ATTR_CASE => PDO::CASE_LOWER, PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION)*/);
            $con->query("SET NAMES utf8; SELECT @aes_key_for_passwd:='39IsoQcrzyblPAEZ';");
            return $con;
//        } catch (PDOException $e) {
//            echo $e->getMessage();
//            return $e->getMessage();
//
//        }
    }

    /**
     * Conexao::fastQuery($query : string [, $arguments : array [, $returnException : boolean]]) : array
     *
     * Função util pra caramba para queries rápidas de qualquer tipo. recebe uma query com ou sem coringas e pode ou não receber mais arguentos em uma lista, contendo os valores dos coringas
     *
     * @param string $query A query SQL para executar.
     * @param array $arguments Uma lista opcional contendo possíveis valores coringas para a query, repassada para o PDOStatement::execute([$arg : array]);
     * @param bool $returnException Booleano opcional para retornar ou não uma possivel exceção PDOException.
     * @return array | boolean | PDOException Array contendo a lista/tabela retornada pelo SGBD em caso de sucesso. Ao falhar, é retornada a exceção PDOException ou um booleano false, de acordo com o parâmetro $returnException.
     */
    public function fastQuery($query, $arguments = [], $returnException = false) {
        try{
            $connection = $this->conecta();
            $PDO = $connection->prepare($query);
            $PDO->execute($arguments);
            $connection = null;

            if ($PDO->rowCount() > 0) {
                return $PDO->fetchAll();
            }else{
                return [];
            }
        }catch (PDOException $e) {
            if ($returnException)
                return $e;
            else
                return false;
        }
    }

    public static function freeQuery($query, $arguments = [], $returnException = false, $user = 'root', $senha = '', $db = 'projeto_cia', $host = 'localhost', $port = "3306") {
        return (new Conexao($user, $senha, $db, $host, $port))->fastQuery($query, $arguments, $returnException);
    }
}


