<?php
require_once "classes/Conexao.php";

class Auth
{


    /**
     * Auth constructor.
     */
    public function __construct()
    {
    }

    public static function auth_user($login, $password)
    {
        $auth_query = Conexao::freeQuery("SELECT * FROM usuario where (CPF_Usuario = ? or Email_Usuario = ? or Usuario_Usuario = ?) and (Senha_Usuario = ?)", [$login, $login, $login, $password]);

        return $auth_query?
            true:
            false;
    }

    public static function _auth_user($login, $password)
    {
        $auth_query = Conexao::freeQuery("SELECT * FROM usuario where (CPF_Usuario = ? or Email_Usuario = ? or Usuario_Usuario = ?) and (Senha_Usuario = ?)", [$login, $login, $login, $password]);

        return $auth_query;
    }
}