-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 19-Nov-2019 às 15:03
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "-04:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projeto_cia`
--
CREATE DATABASE IF NOT EXISTS `projeto_cia` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `projeto_cia`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_usuario`
--
-- Criação: 05-Set-2019 às 05:22
--

DROP TABLE IF EXISTS `tipo_usuario`;
CREATE TABLE `tipo_usuario` (
  `Id_Tipo_Usuario` int(10) NOT NULL,
  `Tipo_Usuario_Atributo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONSHIPS FOR TABLE `tipo_usuario`:
--

--
-- Indexes for table `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`Id_Tipo_Usuario`);

--
-- Truncate table before insert `tipo_usuario`
--

TRUNCATE TABLE `tipo_usuario`;
--
-- Extraindo dados da tabela `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`Id_Tipo_Usuario`, `Tipo_Usuario_Atributo`) VALUES
(1, 'Cuidador de Pessoa Idosa - CPI'),
(2, 'Familiar');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--
-- Criação: 05-Set-2019 às 05:22
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `Id_Usuario` int(10) NOT NULL,
  `Nome_Usuario` varchar(60) NOT NULL,
  `CPF_Usuario` varchar(11) NOT NULL,
  `Data_Nasc_Usuario` date NOT NULL,
  `Usuario_Usuario` varchar(20) NOT NULL,
  `Senha_Usuario` varchar(10) NOT NULL,
  `Email_Usuario` varchar(40) NOT NULL,
  `Tipo_Usuario_Id_Tipo_Usuario` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONSHIPS FOR TABLE `usuario`:
--   `Tipo_Usuario_Id_Tipo_Usuario`
--       `tipo_usuario` -> `Id_Tipo_Usuario`
--

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`Id_Usuario`),
  ADD KEY `fk_Usuario_Tipo_Usuario1_idx` (`Tipo_Usuario_Id_Tipo_Usuario`);
  
  --
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_Usuario_Tipo_Usuario1` FOREIGN KEY (`Tipo_Usuario_Id_Tipo_Usuario`) REFERENCES `tipo_usuario` (`Id_Tipo_Usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;
  
--
-- Truncate table before insert `usuario`
--

TRUNCATE TABLE `usuario`;
--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`Id_Usuario`, `Nome_Usuario`, `CPF_Usuario`, `Data_Nasc_Usuario`, `Usuario_Usuario`, `Senha_Usuario`, `Email_Usuario`, `Tipo_Usuario_Id_Tipo_Usuario`) VALUES
(1, 'José Coimbra Freire Neto', '029.152.912', '2000-12-12', 'josecoimbra12', '12121221', 'josecoimbra12@gmail.com', 1),
(2, 'Frenanda Macara', '111.111.111', '8222-05-05', 'nandeca', 'jdijsd', 'nandeca24@gmail.com', 2),
(3, 'testesss', '029.112.738', '1211-12-12', 'zezecoco', 'aaaa', 'cucucu@cu.com', 2),
(4, 'Sebastião da Silva Sauro', '39402348294', '4333-03-04', 'egdfbfd', 'ssss', 'cucucu@cu.com', 1),
(5, 'Zé da Unção', '55555777777', '1990-09-10', 'zee', '12345', 'josecoimbra123@gmail.com', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `idoso`
--
-- Criação: 05-Set-2019 às 05:22
--

DROP TABLE IF EXISTS `idoso`;
CREATE TABLE `idoso` (
  `Id_Idoso` int(10) NOT NULL,
  `Id_Familiar` int(10) NOT NULL,
  `Id_CPI` int(10) NOT NULL,
  `Nome_Idoso` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONSHIPS FOR TABLE `idoso`:
--   `Id_CPI`
--       `usuario` -> `Id_Usuario`
--   `Id_Familiar`
--       `usuario` -> `Id_Usuario`
--

--
-- Indexes for table `idoso`
--
ALTER TABLE `idoso`
  ADD PRIMARY KEY (`Id_Idoso`),
  ADD KEY `fk_Idoso_Usuario1_idx` (`Id_CPI`),
  ADD KEY `fk_Idoso_Usuario2_idx` (`Id_Familiar`);

--
-- Limitadores para a tabela `idoso`
--
ALTER TABLE `idoso`
  ADD CONSTRAINT `fk_Idoso_Usuario1` FOREIGN KEY (`Id_CPI`) REFERENCES `usuario` (`Id_Usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Idoso_Usuario2` FOREIGN KEY (`Id_Familiar`) REFERENCES `usuario` (`Id_Usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Truncate table before insert `idoso`
--

TRUNCATE TABLE `idoso`;
--
-- Extraindo dados da tabela `idoso`
--

INSERT INTO `idoso` (`Id_Idoso`, `Id_Familiar`, `Id_CPI`, `Nome_Idoso`) VALUES
(1, 5, 1, 'Jorbiscleidson Cunha'),
(2, 3, 1, 'Eldenilson'),
(3, 2, 4, 'Maria silva'),
(4, 3, 4, 'coimbrdddddddddd');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_comportamento`
--
-- Criação: 05-Set-2019 às 05:22
--

DROP TABLE IF EXISTS `tipo_comportamento`;
CREATE TABLE `tipo_comportamento` (
  `Id_Tipo_Comportamento` int(10) NOT NULL,
  `Nome_Comportamento` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONSHIPS FOR TABLE `tipo_comportamento`:
--

--
-- Indexes for table `tipo_comportamento`
--
ALTER TABLE `tipo_comportamento`
  ADD PRIMARY KEY (`Id_Tipo_Comportamento`);

--
-- Truncate table before insert `tipo_comportamento`
--

TRUNCATE TABLE `tipo_comportamento`;
--
-- Extraindo dados da tabela `tipo_comportamento`
--

INSERT INTO `tipo_comportamento` (`Id_Tipo_Comportamento`, `Nome_Comportamento`) VALUES
(1, 'Estressado'),
(2, 'Calmo'),
(3, 'Emotivo'),
(4, 'Hiperativo'),
(5, 'Ansioso');

-- --------------------------------------------------------

--
-- Estrutura da tabela `comportamento`
--
-- Criação: 11-Set-2019 às 19:54
--

DROP TABLE IF EXISTS `comportamento`;
CREATE TABLE `comportamento` (
  `Id_Comportamento` int(10) NOT NULL,
  `OBS_Humor_Diario` varchar(100) NOT NULL,
  `Idoso_Id_Idoso` int(10) NOT NULL,
  `Tipo_Comportamento_Id_Tipo_Comportamento` int(10) NOT NULL,
  `Data_Comportamento` date NOT NULL,
  `Turno_Comportamento` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONSHIPS FOR TABLE `comportamento`:
--   `Idoso_Id_Idoso`
--       `idoso` -> `Id_Idoso`
--   `Tipo_Comportamento_Id_Tipo_Comportamento`
--       `tipo_comportamento` -> `Id_Tipo_Comportamento`
--

--
-- Indexes for table `comportamento`
--
ALTER TABLE `comportamento`
  ADD PRIMARY KEY (`Id_Comportamento`),
  ADD KEY `fk_Comportamento_Idoso1_idx` (`Idoso_Id_Idoso`),
  ADD KEY `fk_Comportamento_TipoComportamento1_idx` (`Tipo_Comportamento_Id_Tipo_Comportamento`);

--
-- Limitadores para a tabela `comportamento`
--
ALTER TABLE `comportamento`
  ADD CONSTRAINT `fk_Comportamento_Idoso1` FOREIGN KEY (`Idoso_Id_Idoso`) REFERENCES `idoso` (`Id_Idoso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Comportamento_TipoComportamento1` FOREIGN KEY (`Tipo_Comportamento_Id_Tipo_Comportamento`) REFERENCES `tipo_comportamento` (`Id_Tipo_Comportamento`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Truncate table before insert `comportamento`
--

TRUNCATE TABLE `comportamento`;
--
-- Extraindo dados da tabela `comportamento`
--

INSERT INTO `comportamento` (`Id_Comportamento`, `OBS_Humor_Diario`, `Idoso_Id_Idoso`, `Tipo_Comportamento_Id_Tipo_Comportamento`, `Data_Comportamento`, `Turno_Comportamento`) VALUES
(1, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 1, 1, '2019-09-11', 'Dia todo'),
(2, 'a2aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 1, 5, '2019-09-11', 'Tarde'),
(3, 'a2aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 1, 5, '2019-09-11', 'Tarde'),
(4, 'aaaaaaaaqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', 1, 3, '2019-09-11', 'Manhã'),
(5, 'aaaaaaaaqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', 1, 4, '2019-09-05', 'Tarde'),
(6, 'aa111111111111111111aaaaaaqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', 1, 4, '2019-09-08', 'Dia todo'),
(7, 'aa111111111111111111aaaaaaqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', 1, 4, '2019-09-10', 'Noite'),
(8, 'nanda kore wa', 1, 5, '2019-09-12', 'Tarde'),
(9, 'sdfsdfsdf', 1, 1, '2019-09-12', 'Manhã'),
(10, 'oioi', 1, 5, '2019-09-12', 'Tarde'),
(11, 'rtfctcftcfcft', 1, 3, '2019-09-12', 'Manhã'),
(12, 'oioioio', 1, 5, '2019-09-25', 'Tarde'),
(13, '', 1, 5, '2019-09-12', 'Dia todo'),
(14, '', 1, 4, '2019-09-12', 'Manhã'),
(15, 'Estava fora do normal', 1, 4, '2019-09-16', 'Dia todo'),
(16, '', 1, 1, '2019-09-27', 'Noite'),
(17, 'viu um filme emocionante', 1, 3, '2019-09-12', 'Noite'),
(18, '', 1, 2, '2019-09-20', 'Manhã'),
(19, '', 1, 3, '2019-09-27', 'Manhã'),
(20, '', 1, 2, '2019-09-14', 'Manhã'),
(21, '', 1, 2, '2019-11-07', 'Noite'),
(22, 'fjwjlj', 1, 1, '2019-11-07', 'Manhã'),
(23, 'hgfhgfhgfhfhfhgfhgfhgfhgf', 1, 2, '2019-11-08', 'Tarde'),
(24, 'Ele estava muito agitado hoje', 1, 4, '2019-11-08', 'Tarde');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_rotina`
--
-- Criação: 19-Set-2019 às 13:45
--

DROP TABLE IF EXISTS `tipo_rotina`;
CREATE TABLE `tipo_rotina` (
  `Id_Tipo_Rotina` int(10) NOT NULL,
  `Nome_Tipo_Rotina` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONSHIPS FOR TABLE `tipo_rotina`:
--

--
-- Indexes for table `tipo_rotina`
--
ALTER TABLE `tipo_rotina`
  ADD PRIMARY KEY (`Id_Tipo_Rotina`);

--
-- Truncate table before insert `tipo_rotina`
--

TRUNCATE TABLE `tipo_rotina`;
--
-- Extraindo dados da tabela `tipo_rotina`
--

INSERT INTO `tipo_rotina` (`Id_Tipo_Rotina`, `Nome_Tipo_Rotina`) VALUES
(1, 'Alarme'),
(2, 'Rotina');

-- --------------------------------------------------------

--
-- Estrutura da tabela `rotina`
--
-- Criação: 19-Set-2019 às 15:29
--

DROP TABLE IF EXISTS `rotina`;
CREATE TABLE `rotina` (
  `Id_Rotina` int(10) NOT NULL,
  `Nome_Rotina` varchar(60) NOT NULL,
  `Hora_Rotina` time NOT NULL,
  `Ciclo_Remedio` int(10) DEFAULT NULL,
  `Tipo_Rotina_Id_Tipo_Rotina` int(10) NOT NULL,
  `Idoso_Id_Idoso` int(10) NOT NULL,
  `Descricao_Rotina` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONSHIPS FOR TABLE `rotina`:
--   `Idoso_Id_Idoso`
--       `idoso` -> `Id_Idoso`
--   `Tipo_Rotina_Id_Tipo_Rotina`
--       `tipo_rotina` -> `Id_Tipo_Rotina`
--

--
-- Indexes for table `rotina`
--
ALTER TABLE `rotina`
  ADD PRIMARY KEY (`Id_Rotina`),
  ADD KEY `fk_Rotina_Tipo_Rotina1_idx` (`Tipo_Rotina_Id_Tipo_Rotina`),
  ADD KEY `fk_Rotina_Idoso1_idx` (`Idoso_Id_Idoso`);

--
-- Limitadores para a tabela `rotina`
--
ALTER TABLE `rotina`
  ADD CONSTRAINT `fk_Rotina_Idoso1` FOREIGN KEY (`Idoso_Id_Idoso`) REFERENCES `idoso` (`Id_Idoso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Rotina_Tipo_Rotina1` FOREIGN KEY (`Tipo_Rotina_Id_Tipo_Rotina`) REFERENCES `tipo_rotina` (`Id_Tipo_Rotina`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Truncate table before insert `rotina`
--

TRUNCATE TABLE `rotina`;
-- --------------------------------------------------------

--
-- Estrutura da tabela `rotina_idoso`
--
-- Criação: 05-Set-2019 às 05:22
--

DROP TABLE IF EXISTS `rotina_idoso`;
CREATE TABLE `rotina_idoso` (
  `Id_Rotina_Idoso` int(11) NOT NULL,
  `Data_Rotina` date NOT NULL,
  `Status_Rotina` tinyint(4) NOT NULL,
  `Idoso_Id_Idoso` int(10) NOT NULL,
  `Rotina_Id_Rotina` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONSHIPS FOR TABLE `rotina_idoso`:
--   `Idoso_Id_Idoso`
--       `idoso` -> `Id_Idoso`
--   `Rotina_Id_Rotina`
--       `rotina` -> `Id_Rotina`
--

--
-- Indexes for table `rotina_idoso`
--
ALTER TABLE `rotina_idoso`
  ADD PRIMARY KEY (`Id_Rotina_Idoso`),
  ADD KEY `fk_Rotina_Idoso_Idoso1_idx` (`Idoso_Id_Idoso`),
  ADD KEY `fk_Rotina_Idoso_Rotina1_idx` (`Rotina_Id_Rotina`);

--
-- Limitadores para a tabela `rotina_idoso`
--
ALTER TABLE `rotina_idoso`
  ADD CONSTRAINT `fk_Rotina_Idoso_Idoso1` FOREIGN KEY (`Idoso_Id_Idoso`) REFERENCES `idoso` (`Id_Idoso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Rotina_Idoso_Rotina1` FOREIGN KEY (`Rotina_Id_Rotina`) REFERENCES `rotina` (`Id_Rotina`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Truncate table before insert `rotina_idoso`
--

TRUNCATE TABLE `rotina_idoso`;






--
-- Indexes for dumped tables
--

-- Moved them already!

















--
-- AUTO_INCREMENT for dumped tables
--
/*
--
-- AUTO_INCREMENT for table `comportamento`
--
ALTER TABLE `comportamento`
  MODIFY `Id_Comportamento` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `idoso`
--
ALTER TABLE `idoso`
  MODIFY `Id_Idoso` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rotina`
--
ALTER TABLE `rotina`
  MODIFY `Id_Rotina` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rotina_idoso`
--
ALTER TABLE `rotina_idoso`
  MODIFY `Id_Rotina_Idoso` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tipo_comportamento`
--
ALTER TABLE `tipo_comportamento`
  MODIFY `Id_Tipo_Comportamento` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tipo_rotina`
--
ALTER TABLE `tipo_rotina`
  MODIFY `Id_Tipo_Rotina` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  MODIFY `Id_Tipo_Usuario` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `Id_Usuario` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
*/

--
-- Constraints for dumped tables
--

-- Moved them already!









/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
