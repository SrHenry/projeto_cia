<!DOCTYPE html>
<html>
<head>
	  <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="bootstrap-4.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="normalize.css">
    <link rel="stylesheet" href="padrao_cia_stylesheet.css">
    <link rel="stylesheet" href="padrao_comportamento.css">

    <!--
		$con_a = $link->query("SELECT * FROM comportamento WHERE Idoso_id_Idoso = $idIdoso AND Data_Comportamento >= $data1 AND Data_Comportamento <= $data2;");
		$humores = array();
		while ($row = $con_a->fetch_array()) {
    		$comportamento = $row['Tipo_Comportamento_Id_Tipo_Comportamento'];
    		$humores[count($humores)] = $link->query("SELECT Nome_Comportamento from tipo_comportamento WHERE Id_Tipo_Comportamento = $comportamento;")->fetch_array()['Nome_Comportamento'];

		}

    	//SELECT * FROM comportamento WHERE Idoso_id_Idoso = $idIdoso AND Data_Comportamento >= $data1 AND Data_Comportamento <= $data2;

    	//SELECT Nome_Comportamento from tipo_comportamento WHERE Id_Tipo_Comportamento = $comportamento;
    --> 
    <?php 
    	require_once("conexao.php");

    	echo "<script>console.log(\"\$_POST:\", ".json_encode($_POST).");</script>";

    	$con_a = $link->query("SELECT * FROM comportamento WHERE Idoso_id_Idoso = 1 AND Data_Comportamento >= '".(isset($_POST['data1'])?$_POST['data1']: date("Y-m-d"))."' AND Data_Comportamento <= '".(isset($_POST['data2'])?$_POST['data2']: date("Y-m-d"))."';");
		$humores = array();
		while ($row = $con_a->fetch_array()) {
    		$comportamento = $row['Tipo_Comportamento_Id_Tipo_Comportamento'];
    		$humores[count($humores)] = $link->query("SELECT Nome_Comportamento from tipo_comportamento WHERE Id_Tipo_Comportamento = $comportamento;")->fetch_array()['Nome_Comportamento'];

		}

		echo "<script>console.log(\"\$humores:\", ".json_encode($humores).");</script>";

    	//SELECT * FROM comportamento WHERE Idoso_id_Idoso = $idIdoso AND Data_Comportamento >= $data1 AND Data_Comportamento <= $data2;
    	
    	//SELECT Nome_Comportamento from tipo_comportamento WHERE Id_Tipo_Comportamento = $comportamento;
    ?>

    <!--[if IE 9]>
    <script src="html5shiv.js"></script>
    <![endif]-->
    <title>CIA - Login</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
    <!--script src="http://digitalbush.com/files/jquery/maskedinput/rc3/jquery.maskedinput.js" type="text/javascript"></script-->
    <!--script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
    	//carregando modulo visualization
      google.load("visualization", "1", {packages:["corechart"]});
       
      //função de monta e desenha o gráfico
      function drawChart() {
	    //variavel com armazenamos os dados, um array de array's 
	    //no qual a primeira posição são os nomes das colunas
	    /*var data = google.visualization.arrayToDataTable([
          ['Linguagem', 'Quando gosto dela'],
          ['Java',     40],
          ['PHP',      30],
          ['Javascript',  25],
          ['C#', 5],
	           
        ]);*/

	    /*
	    	<?php
	    		//$vetor = array(["Humores", "Humores do idoso entre as datas"]);
	    		//$linha = [];
	    		//while ($row = $link->query("SELECT Nome_Comportamento from tipo_comportamento;")->fetch_array()) {
					//$linha[$row['Nome_Comportamento']] = 0;
	    		//}
	    		//foreach ($linha as $_humor) {
	    			//foreach ($humores as $humor){
						//acalcula ai po
						//if ($humor == $_humor)
							//$linha[$_humor] += 1;
	    			//}
	    			//$vetor[count($vetor)] = linha[$_humor];
	    		//}
	    		//echo "let show_me = ".json_encode($vetor);
	    	?>;

			var data = google.visualization.arrayToDataTable(show_me);
	    */

	    <?php
	    		$vetor = array(["Humores", "Humores do idoso entre as datas"]);
	    		$linha = [];
	    		$con_b = $link->query("SELECT Nome_Comportamento from tipo_comportamento;");
	    		while ($row = $con_b->fetch_array()) {
					$linha[$row['Nome_Comportamento']] = 0;
	    		}
	    		echo "console.log('\$linha (before):', ".json_encode($linha).");\n";
	    		foreach ($linha as $_humor => $porcentagem) {
	    			foreach ($humores as $humor){
						if ($humor == $_humor)
							$linha[$_humor] = $linha[$_humor] + 1;
	    			}
	    			$vetor[count($vetor)] = [$_humor, $linha[$_humor]];
	    		}
	    		echo "console.log('\$linha (after):', ".json_encode($linha).");\n";
	    		echo "console.log('\$vetor:', ".json_encode($vetor).");\n";
	    		echo "let show_me = ".json_encode($vetor);
	    	?>;

			var data = google.visualization.arrayToDataTable(show_me);

        //opções para exibição do gráfico
        var options = {
                title: 'Humor do Idoso',//titulo do gráfico
        is3D: true // false para 2d e true para 3d o padrão é false
        };
        //cria novo objeto PeiChart que recebe 
        //como parâmetro uma div onde o gráfico será desenhado
        var chart = new google.visualization
        .PieChart(document.getElementById('chart_div'));
        //desenha passando os dados e as opções
        chart.draw(data, options);
      }

      google.setOnLoadCallback(drawChart);
    </script>
	<title></title>
</head>
<body>
	<nav id="menu">
        <ul >
            <li><a href="paginaInicial.html">Home</a></li>
            <li><a href="#">Menu</a></li>
        </ul>
    </nav>
    <div class="container ">
    	<form style="position: relative; top:100px;" method="post">
    		<div class="row mt-3 ">
  				<div class="col-12">
	    			<center> <h5>Adicione a avaliação diária do comportamento do Idoso: </h5> </center>
	    		</div>
	    		<div class="col-12">
	    			<a href="comportamento_familiar_inserirHumor.php"> <button type="button" class="btn btn-lg btn-block btn-success" id="botao_mudar_pag">Adicionar Humor do dia </button></a><br/><br/><br/>
	    		</div>
	    	</div>
	    	<div class="row mt-3 ">
	    		<div class="col-12">
	    			<center> <h5>Consulte o comportamento do Idoso no período: </h5> </center>
	    		</div>
	    	</div>

	    	<div class="row">
	    		<div class="col-6">
	    			<label for="data1">De:</label>
            		<input type="Date" class="form-control" id="data1" name="data1" value="<?php date_default_timezone_set("America/Porto_Velho"); if (isset($_POST['data1'])) echo $_POST['data1']; else echo date("Y-m-d");?>" placeholder="Observações sobre o comportamento do idoso">
                </div>
                <div class="col-6">
	                <label for="data2">Até:</label>
	                <input type="Date" class="form-control" id="data2" name="data2" value="<?php if (isset($_POST['data2'])) echo $_POST['data2']; else echo date("Y-m-d");?>" placeholder="Observações sobre o comportamento do idoso">
	    		</div>
	    	</div>
	    	<div class="row mt-3">
	    		<div class="col-12">
	    			<button type="submit" class="btn btn-lg btn-block btn-success" id="botao_confirmar">Gerar Gráfico</button>
	    			 <div id="chart_div" class="mt-3"></div>
	    		</div>
	    	</div>
	    	<div class="row">
	    		<div class="col-12">
	    			<div id="columnchart_material" style="width: 800px; height: 500px;"></div>
	    		</div>
	    	</div>
	    </form>
   	</div>
   	
</body>
</html>