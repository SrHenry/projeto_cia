<?php
session_start();

if (!isset($_SESSION['usuario']['active']) || !$_SESSION['usuario']['active'])
    header("Location: index.php");
?>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="bootstrap-4.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="normalize.css">
    <link rel="stylesheet" href="padrao_cia_stylesheet.css">
    <link rel="stylesheet" href="alarme_stylesheet.css">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>

    <!--[if IE 9]>
    <script src="html5shiv.js"></script>
    <![endif]-->
    <title>CIA</title>

    <script type="text/javascript">
        var colorList = {
            count: {
                c : 0,
                next: function() {
                    if (this.c < colorList.colors.length-1)
                        return ++(this.c);
                    else {
                        return (this.c = 0);
                    }
                }
            },
            colors: [
                "#fa6879",
                "#3bb964"
            ],
            nextColor: function() {
                return this.colors[this.count.next()];
            }
        };
        function selecionar(k){
            var x = document.getElementsByClassName("camaleao");
            k.style.background = colorList.nextColor();

//            var cor="#fa6879";
//            if( cor === "#fa6879"){
//                document.getElementById(k).style.background = "#3bb964";
//            };
//            if( x === "blue"){
//                document.getElementById(k).style.background = "red";
//            };
        }
    </script>
</head>
<body>
<nav id="menu">
    <ul >
        <li><a href="#">Home</a></li>
        <li><a href="#">Home</a></li>
        <li><a href="#">Menu</a></li>
    </ul>
</nav>


<div class="container">
    <div class="col-12">
        <div id="time"></div>
    </div>

    <!-- janela-->
    <div class="modal fade" id="janela">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <!-- cabeçalho-->
                <div class="modal-header">
                    <h3 class="modal-title"> Insira novo alarme</h3>
                    <button class="close" data-dismiss="modal">
                        <span> &times; </span>
                    </button>
                    <div id="corum">
                    </div>
                    <div id="cordois">
                    </div>
                    <div id="cortres">
                    </div>
                    <div id="corquatro">
                    </div>
                    <div id="corum">
                    </div>
                    <div id="cordois">
                    </div>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="Nome_Remedio">Nome Remédio:</label>
                            <input type="teste" class="form-control" id="Nome_Remedio" placeholder="Digite o nome do remédio">
                        </div>

                        <div class="form-group">
                            <label for="Ciclo_Remedio">Ciclo remédio (Em horas):</label>
                            <input type="number" class="form-control" id="Ciclo_Remedio" placeholder="Digite o ciclo do remédio em horas">
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-block btn-outline-success">Inserir</button>

                </div>
            </div>
        </div>
    </div>

    <?php if (isset($_SESSION['usuario']['deny']) && !empty($_SESSION['usuario']['deny']) && in_array("CreateAlarme", $_SESSION['usuario']['deny'])) {}else{?>
    <div class="col-12">
        <div class="tela">
            <button type="button" class="button instagram adicionar_alarme " data-toggle="modal" data-target="#janela" ><span class="gradient"></span>&#43; Adicionar alarme</button>
        </div>
    </div>
    <?php }?>




        <div class="alarme">
            <table id="tabelaAlarme" >
                <tr>
                    <th style="width:95%"></th>
                    <th style="width:5%" ></th>

                </tr>
                <tr>
                    <td> <div class="nome_remedio">
                            <h2>Nome Remédio:</h2>
                            <h1>O tal do remédio</h1>
                        </div></td>
                    <td rowspan="2" ><div class="Status_remedio ">
                            <h2>Status:</h2>
                            <div onclick="selecionar(this);" class="camaleao" style="cursor:pointer; height:100px;width:100px;background:#fa6879;"></div>
                        </div></td>
                </tr>
                <tr>
                    <td ><div class="tempo_remedio">
                            <h2>Ciclo remédio:</h2>
                            <h1>30 horas</h1>
                        </div></td>


                </tr>
            </table>
           <!-- <div class="row">
                <div class="col-11">
                    <div class="nome_remedio">
                        <h2>Nome Remédio:</h2>
                        <h1>O tal do remédio</h1>
                    </div>
                </div>
                <div class="col-1">
                    <div class="Status_remedio">
                        <div onclick="selecionar(this);" class="camaleao" style="cursor:pointer; height:80px;width:80px;background:#fa6879;"></div>
                    </div>
                </div>
            </div>
            <div class="col-11">
                <div class="tempo_remedio">
                    <h2>Ciclo remédio:</h2>
                    <h1>30 horas</h1>
                </div>
            </div>
        </div> -->

</div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>
</html>
<script>
    function checkTime(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        // add a zero in front of numbers<10
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
        t = setTimeout(function () {
            startTime()
        }, 500);
    }
    startTime();
</script>