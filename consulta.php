<?php

include("conexao.php");

$consulta = "SELECT * FROM usuario";
$con = $link->query($consulta) or die($link->error);
?>
<html>
	<head>
		<meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	    <!-- Bootstrap CSS -->
	    <link href="bootstrap-4.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
	    <link rel="stylesheet" href="normalize.css">
	    <link rel="stylesheet" href="padrao_cia_stylesheet.css">			
	   
	</head>
	<body>

		 <nav id="menu">
	        <ul >
	            <li><a href="login_id_idoso.php">Voltar</a></li>
	        </ul>
	    </nav>
	    <br /> <br />  <br />
		<div id=" mx-auto tabelaConsulta"> 
			<table  class="table table-striped " >
				<tr >
					<td scope="col">ID Usuário</td>
					<td scope="col">Tipo Usuário</td>
					<td scope="col">Nome Usuário</td>
					<td scope="col">CPF Usuário</td>
					<td scope="col">Data de Nascimento Usuário</td>
					<td scope="col">Login Usuário</td>
					<td scope="col">Senha Usuário</td>
					<td scope="col">Email Usuário</td>
				</tr>
				<?php while ($dado = $con-> fetch_array()){ ?>
					<tr>
						<td><?php echo $dado["Id_Usuario"]; ?></td>
						<td><?php echo $dado["Tipo_Usuario_Id_Tipo_Usuario"]; ?></td>
						<td><?php echo $dado["Nome_Usuario"]; ?></td>
						<td><?php echo substr($dado["CPF_Usuario"], 0, 3).".".substr($dado["CPF_Usuario"], 3, 3).".".substr($dado["CPF_Usuario"], 6, 3)."-".substr($dado["CPF_Usuario"], 9); ?></td>
						<td><?php echo $dado["Data_Nasc_Usuario"]; ?></td>
						<td><?php echo $dado["Usuario_Usuario"]; ?></td>
						<td><?php echo $dado["Senha_Usuario"]; ?></td>
						<td><?php echo $dado["Email_Usuario"]; ?></td>
					</tr>
				<?php } ?>
			</table>
		 </div>
	
		  <label for="nome">Pesquise por Nome</label>
          <input type="text" class="form-control" id="nome" name="Nome_Usuario"  placeholder="Digite o Nome">
    	  <div class="col-2 formulario"> <a href="select_name.php"><button type="submit" class="btn btn-lg btn-block btn-success" id="botao_cancelar">Pesquisar</button><a/></div>
<br />
<br />

	<div>    
	
	<div id="corum">
	</div>
	<div id="cordois">
	</div>
	<div id="cortres">
	</div>
	<div id="corquatro">
	</div>
	
	</body>
</html>